package com.tool.sms.fileprocessing.controller;

import com.tool.sms.fileprocessing.constant.AppConst;
import com.tool.sms.fileprocessing.dto.RequestDTO;
import com.tool.sms.fileprocessing.entity.ResultEntity;
import com.tool.sms.fileprocessing.service.impl.FileServiceImpl;
import com.tool.sms.fileprocessing.utils.ExcelBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Hashtable;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping(path = "/api/v1")
public class SmsController extends BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SmsController.class);
    private final FileServiceImpl fileService;
    public SmsController(FileServiceImpl fileService) {
        this.fileService = fileService;
    }

    @GetMapping(path = "/send-sms")
    public ResponseEntity<?> sendSms (@RequestParam Optional<Boolean> isEncrypt) {
        String typeSendSms = !isEncrypt.orElseGet(() -> false) ? "plain text" : "cipher text";
        LOGGER.info("Send sms with {}", typeSendSms);
        try {
//            Hashtable<String, List<RequestDTO>> htbRequest = fileService.readFiles(AppConst.SENT_DIRECTORY_PATH);
//            Set<String> keys = htbRequest.keySet();
//            for (String key: keys) {
//                fileService.handleProcessing(key, htbRequest.get(key).subList(1, 10), AppConst.SIZE_THREAD);
//            }
            ExcelBuilder.export();
            return response(new ResultEntity(), HttpStatus.OK);
        }
        catch (Exception ex) {
            LOGGER.error("Error: {}", ex.getMessage());
            return this.response(error(ex), HttpStatus.OK);
        }
    }
}
