package com.tool.sms.fileprocessing.controller;

import com.tool.sms.fileprocessing.entity.ResultEntity;
import com.tool.sms.fileprocessing.exception.IErrorResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;

@Component
@CrossOrigin("*")
public class BaseController implements IErrorResult {
    protected ResponseEntity<?> response(ResultEntity result, HttpStatus code) {
        return new ResponseEntity<>(result, code);
    }
}
