package com.tool.sms.fileprocessing.utils;

import com.google.gson.ExclusionStrategy;
import com.google.gson.GsonBuilder;
import com.tool.sms.fileprocessing.anotation.JsonExclusionStrategy;
import com.tool.sms.fileprocessing.constant.AppConst;
import com.tool.sms.fileprocessing.converter.LocalDateTimeJsonConverter;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class GsonUtils {
    private static GsonUtils instance;
    private GsonUtils() {};
    public static GsonUtils getInstance () {
        if(instance == null) {
            synchronized (GsonUtils.class) {
                if(instance == null) {
                    instance = new GsonUtils();
                }
            }
        }
        return instance;
    }

    public <T> String toJson (T object) {
        ExclusionStrategy exclusionStrategy = new JsonExclusionStrategy();
        return new GsonBuilder()
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeJsonConverter())
                .setExclusionStrategies(exclusionStrategy)
                .setPrettyPrinting()
                .create()
                .toJson(object);
    }

    public <T> T toObject(String json, Class<T> cls) {
        return new GsonBuilder()
                .setDateFormat(AppConst.DATE_TIME_FORMAT)
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeJsonConverter())
                .create()
                .fromJson(json, cls);
    }
}
