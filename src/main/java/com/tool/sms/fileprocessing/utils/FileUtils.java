package com.tool.sms.fileprocessing.utils;

import com.tool.sms.fileprocessing.dto.RequestDTO;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileUtils {
    private static FileUtils instance;
    private FileUtils () {};
    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtils.class);
    static class Lock {
        private Lock()  {};
    }
    private Lock lock = new Lock();
    public static FileUtils getInstance() {
        if(instance == null) {
            synchronized (Lock.class) {
                if(null == instance) {
                    instance = new FileUtils();
                }
            }
        }
        return instance;
    }

    public List<File> listFilesUsingJavaIO(String path) {
        LOGGER.info("Path directory: {}", path);
        File dir = new File(path);
        List<File> files = new ArrayList<>();
        if(!dir.exists() || null == dir.listFiles()) return files;
        LOGGER.info("Directory exists");
        return Stream.of(dir.listFiles())
                .filter(file -> !file.isDirectory())
                .collect(Collectors.toList());
    }

    public CompletableFuture<List<RequestDTO>> readFileExcel (File excel) {
        return CompletableFuture.supplyAsync(() -> {
            LOGGER.info("Begin read file : {}", excel.getName());
            try (
                    InputStream is = new FileInputStream(excel);
                    Workbook workbook = new XSSFWorkbook(is)
            ) {
                Sheet sheet = workbook.getSheetAt(0);
                Iterator<Row> rowIterator = sheet.rowIterator();
                List<RequestDTO> requests = new ArrayList<>();
                int rowNumber = 2;
                while (rowIterator.hasNext()) {
                    if(rowNumber < 3) {
                        rowNumber++;
                        rowIterator.next();
                        continue;
                    }
                    rowNumber++;
                    Row row = rowIterator.next();
                    RequestDTO request = new RequestDTO();
//                    request.setKeyword(row.getCell(0).getStringCellValue());
//                    request.setSender(row.getCell(1).getStringCellValue());
//                    request.setDestination(row.getCell(2).getStringCellValue());
//                    request.setShortMessage(row.getCell(3).getStringCellValue());
//                    requests.add(request);
                }
                LOGGER.info("End read file : {}", excel.getName());
                return requests;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public Path changePathFile (String source, String destination) throws IOException{
        Path srcPath = Paths.get(source);
        Path desPath =  Paths.get(destination);
        return Files.move(srcPath, desPath);
    }
}
