package com.tool.sms.fileprocessing.utils;

import com.tool.sms.fileprocessing.dto.RequestDTO;
import com.tool.sms.fileprocessing.exception.CustomException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.atomic.AtomicInteger;

public class NetworkUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(NetworkUtils.class);

    private AtomicInteger counter = new AtomicInteger(0);

    public int getCounter() {return this.counter.get();}
    public void setCounter() {this.counter.incrementAndGet();}

    private NetworkUtils() {};

    private static final class InstanceHolder {
        private static final NetworkUtils instance = new NetworkUtils();
    }

    public static NetworkUtils getInstance() {
        return InstanceHolder.instance;
    }

    public ResponseEntity<Object> doPost (RequestDTO rq, String url) throws CustomException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
            headers.setContentType(MediaType.APPLICATION_JSON);
            RestTemplate restTemplate = new RestTemplate();
            HttpEntity<Object> requestBody = new HttpEntity<>(rq, headers);
            this.setCounter();
            return restTemplate.postForEntity(url, requestBody, Object.class);
        }
        catch (Exception ex) {
            throw new CustomException(ex);
        }
    }
}
