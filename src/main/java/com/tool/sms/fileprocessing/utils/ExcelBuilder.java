package com.tool.sms.fileprocessing.utils;

import com.tool.sms.fileprocessing.constant.AppConst;
import lombok.extern.slf4j.Slf4j;
import org.dhatim.fastexcel.BorderStyle;
import org.dhatim.fastexcel.VisibilityState;
import org.dhatim.fastexcel.Workbook;
import org.dhatim.fastexcel.Worksheet;
import org.slf4j.MDC;

import java.io.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.CompletableFuture;

/**
 * @author vuongdq1
 * Date: 08/06/2022
 * Time: 10:10
 */

@Slf4j
public class ExcelBuilder {

    private ExcelBuilder() {
        throw new IllegalStateException("ExcelBuilder class");
    }

    public static void export() throws IOException {
        Random random =new Random();
        int rand = random.nextInt(20 - 5) + 5;
        try (OutputStream os = new FileOutputStream(AppConst.ROOT_PATH+ "/tata"+rand+".xlsx", false)) {
            Workbook wb = new Workbook(os, "MyApplication", "1.0");
            Worksheet ws = wb.newWorksheet("Sheet 1");
//            ws.setVisibilityState(VisibilityState.HIDDEN);
//            ws.range(0, 0, 0, 1).merge();
//            ws.range(6, 0, 7, 2).merge();
//            ws.range(13, 0, 13, 1).merge();
//            ws.range(22, 0, 22, 1).merge();
//            ws.range(30, 0, 30, 1).merge();
//            ws.range(39, 0, 39, 1).merge();
//            ws.range(46, 0, 46, 1).merge();
//            ws.range(51, 0, 51, 1).merge();
//            ws.range(51, 3, 51, 4).merge();
//            ws.range(51, 6, 51, 7).merge();
                    ws.range(0, 2, 2, 0).merge();
        ws.value(0, 0, "TRANSACTION MANAGEMENT");
        ws.style(0, 0)
                .bold()
                .fontSize(14)
                .fontColor("FFFFFF")
                .fillColor("00B050")
//                .fontName(TIMES_NEW_ROMAN)
                .verticalAlignment("center")
                .horizontalAlignment("center")
                .borderStyle(BorderStyle.THIN).set();
        ws.rowHeight(0, 22);
        ws.width(0, 20);
        ws.width(1, 20);

            ws.finish();
            wb.finish();
        }
    }

    public static String getFileName(String prefixName, String folderName) {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        return folderName.concat(LocalDateTime.now().format(format))
                .concat(prefixName)
                .concat(new SimpleDateFormat(AppConst.DATE_TIME_FORMAT).format(Calendar.getInstance().getTime()))
                .concat(AppConst.EXCEL_FILE_EXTENSION);
    }

//    public static InputStream exportExcelFileTransactionReport(List<TransactionDTO> list, int sheetSize) throws Exception {
//        log.info("Start export excel use fast excel with size data: {} ", list.size());
//        long startTime = System.currentTimeMillis();
//        try (ByteArrayOutputStream fos = new ByteArrayOutputStream()) {
//            Workbook workbook = new org.dhatim.fastexcel.Workbook(fos, "Application", "1.0");
//            Worksheet wsConfig = workbook.newWorksheet("Config");
//            ArrayList<CompletableFuture<Void>> lstResult = new ArrayList<>();
//            CompletableFuture<Void> completableFutureAddConfig = CompletableFuture.runAsync(() -> createWorksheetConfig(wsConfig));
//            lstResult.add(completableFutureAddConfig);
//            int totalSheet = list.size() / sheetSize + 1;
//            int indexList = 0;
//            int size;
//            if (totalSheet == 1) {
//                size = list.size();
//            } else {
//                size = sheetSize;
//            }
//            for (int i = 0; i < totalSheet; i++) {
//                log.info("Begin write sheet : {}", i);
//                String sheetStr = "Sheet " + (i + 1);
//
//                log.info("Begin export data from {} to {}.", indexList, size);
//                int finalIndexList = indexList;
//                int finalSize = size;
//                CompletableFuture<Void> comparable = CompletableFuture.runAsync(() -> {
//                    exportData(list, workbook, sheetStr, finalIndexList, finalSize);
//                });
//                indexList += sheetSize;
//                if (size + sheetSize > list.size()) {
//                    size += list.size() - size;
//                } else {
//                    size += sheetSize;
//                }
//                lstResult.add(comparable);
//                log.info("End write sheet : {}", i);
//            }
//
//            CompletableFuture.allOf(lstResult.toArray(new CompletableFuture[0])).get();
//            workbook.setActiveTab(1);
//            wsConfig.finish();
//            workbook.finish();
//            log.info("Write output stream to file excel success. Time handler: {}ms", (System.currentTimeMillis() - startTime));
//            return new ByteArrayInputStream(fos.toByteArray());
//        }
//    }

//    private static void exportData(List<TransactionDTO> list, Workbook workbook, String sheetStr, Integer index, Integer size) {
//        log.info("Begin push data to sheet excel");
//        Worksheet ws = workbook.newWorksheet(sheetStr);
//        ws.range(0, 1, 0, 0).merge();
//        ws.value(0, 0, "TRANSACTION MANAGEMENT");
//        ws.style(0, 0)
//                .bold()
//                .fontSize(14)
//                .fontColor("FFFFFF")
//                .fillColor("00B050")
//                .fontName(TIMES_NEW_ROMAN)
//                .verticalAlignment(CENTER)
//                .horizontalAlignment(CENTER)
//                .borderStyle(BorderStyle.THIN).set();
//        ws.rowHeight(0, 22);
//        ws.width(0, 20);
//        ws.width(1, 20);
//
//        // Config header
//        String[] headers = {"NO", "Transaction ID", "Order code", "Bill number", "Service", "Master merchant Code",
//                "Master merchant name", "Terminal code", "Terminal name", "Card type", "Issuing bank", "Customer account",
//                "Customer mobile", "Customer name", "Account type", "QR type", "Debit amount", "Real amount",
//                "Voucher code", "Transaction date", "Transaction status", "Additional data", "International MCC",
//                "Domestic MCC", "Transaction risk score", "Alert indicator", "Rule group", "Case ID", "Case indicator",
//                "Fraud status"};
//        ws.rowHeight(2, 25);
//        for (int i = 0; i < headers.length; i++) {
//            ws.value(2, i, headers[i]);
//            ws.rowHeight(2, 20);
//            ws.width(i, 20);
//            ws.style(2, i)
//                    .bold()
//                    .fontSize(12)
//                    .borderStyle(BorderStyle.THIN)
//                    .fontColor("110100")
//                    .fillColor("FFFF00")
//                    .fontName(TIMES_NEW_ROMAN)
//                    .verticalAlignment(CENTER)
//                    .horizontalAlignment(CENTER)
//                    .set();
//        }
//
//        // Config data
//        int row = 3;
//        for (int rowId = index; rowId < size; rowId++) {
//            for (int col = 0; col < 30; col++) {
//                if (col == 0) { // STT
//                    ws.formula(row, col, "ROW()-ROW(A3)");
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110100")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(CENTER)
//                            .set();
//                } else if (col == 1) { // Transaction ID
//                    ws.value(row, col, list.get(rowId).getTransactionMappingId());
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110100")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(CENTER)
//                            .set();
//                } else if (col == 2) { // Order code
//                    ws.value(row, col, list.get(rowId).getSystemRequest().getOrderCode());
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110101")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .horizontalAlignment(RIGHT)
//                            .verticalAlignment(CENTER)
//                            .set();
//                } else if (col == 3) { // Bill number
//                    ws.value(row, col, list.get(rowId).getSystemRequest().getBillNumber());
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 4) { // Service
//                    ws.formula(row, col, String.format("IFERROR(VLOOKUP(\"%d\", Config!$A$2:$B$5, 2, 0),\"\")",
//                            list.get(rowId).getSystemRequest().getDataSource().getPaymentSource()));
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 5) { // Master merchant Code
//                    ws.value(row, col, list.get(rowId).getSystemRequest().getMerchant().getMerchantCode());
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 6) { // Master merchant name
//                    ws.value(row, col, list.get(rowId).getSystemRequest().getMerchant().getMerchantName());
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 7) { // Terminal code
//                    ws.value(row, col, list.get(rowId).getSystemRequest().getTerminal().getTerminalCode());
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 8) { // Terminal name
//                    ws.value(row, col, list.get(rowId).getSystemRequest().getTerminal().getTerminalName());
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 9) { // Card type
//                    ws.formula(row, col, String.format("IFERROR(VLOOKUP(\"%s\", Config!$A$32:$B$33, 2, 0),\"\")",
//                            list.get(rowId).getSystemRequest().getCardType()));
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 10) { // Issue bank
//                    ws.formula(row, col, String.format("IFERROR(VLOOKUP(%s, Config!$G$53:$H$107, 2, 0),\"\")"
//                            , list.get(rowId).getSystemRequest().getBankCode()));
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 11) { // Customer account
//                    ws.value(row, col, list.get(rowId).getSystemRequest().getCustomer().getCustomerAccountNo());
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 12) { // Customer mobile
//                    ws.value(row, col, list.get(rowId).getSystemRequest().getCustomer().getCustomerMobile());
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 13) { // Customer name
//                    ws.value(row, col, list.get(rowId).getSystemRequest().getCustomer().getFullName());
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 14) { // Account type
//                    ws.formula(row, col, String.format("IFERROR(VLOOKUP(\"%s\", Config!$A$15:$B$21, 2, 0),\"\")",
//                            list.get(rowId).getSystemRequest().getTypeSource()));
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 15) { // QR type
//                    ws.formula(row, col,
//                            String.format("IFERROR(VLOOKUP(\"%s\", Config!$A$41:$B$45, 2, 0),\"\")",
//                                    list.get(rowId).getSystemRequest().getQrTrans().getServiceCode()));
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 16) { // Debit amount
//                    ws.value(row, col, list.get(rowId).getSystemRequest().getAmountAfterDiscount());
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 17) { // Real amount
//                    ws.value(row, col, list.get(rowId).getSystemRequest().getAmountBeforeDiscount());
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 18) { // Voucher code
//                    ws.value(row, col, list.get(rowId).getSystemRequest().getVoucherCode());
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 19) { // Transaction mapping date
//                    ws.value(row, col, Utils.timestamp2Str(list.get(rowId).getTransactionMappingDate()));
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 20) { // Transaction status
//                    ws.value(row, col, list.get(rowId).getSystemRequest().getStatus());
//                    ws.formula(row, col, String.format("IFERROR(VLOOKUP(\"%d\", Config!$A$24:$B$29, 2, 0),\"\")"
//                            , list.get(rowId).getSystemRequest().getStatus()));
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 21) { // Additional data
//                    ws.value(row, col, list.get(rowId).getSystemRequest().getQrTrans().getAdditionalData());
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 22) { // International mcc
//                    ws.formula(row, col,
//                            String.format("IFERROR(VLOOKUP(%s, Config!$A$53:$B$342, 2, 0),\"\")"
//                                    , list.get(rowId).getSystemRequest().getInternationalMcc()));
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 23) { // Domestic mcc
//                    ws.formula(row, col, String.format("IFERROR(VLOOKUP(%s, Config!$D$53:$E$368, 2, 0),\"\")",
//                            list.get(rowId).getSystemRequest().getDomesticMcc()));
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 24) { // Transaction risk score
//                    ws.value(row, col, list.get(rowId).getRiskScore());
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 25) { // Alert indicator
//                    ws.value(row, col, list.get(rowId).getAlertInd());
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 26) { // Rule group
//                    StringBuilder data = new StringBuilder();
//                    if (null != list.get(rowId).getLstRuleGroup()) {
//                        for (RuleGroupDTO ruleGroupDTO : list.get(rowId).getLstRuleGroup()) {
//                            String temp = ruleGroupDTO.getId().toString() + "-" + ruleGroupDTO.getName();
//                            data.append(temp).append(";");
//                        }
//                    }
//                    ws.value(row, col, String.valueOf(data));
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 27) { // Case id
//                    StringBuilder data = new StringBuilder();
//                    if (null != list.get(rowId).getLstCase()) {
//                        for (CaseDTO caseDTO : list.get(rowId).getLstCase()) {
//                            if (null != caseDTO) {
//                                String temp = caseDTO.getId().toString();
//                                data.append(temp).append(";");
//                            }
//                        }
//                    }
//                    ws.value(row, col, String.valueOf(data));
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 28) { // Case indicator
//                    ws.value(row, col, list.get(rowId).getCaseInd());
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                } else if (col == 29) { // Fraud status
//                    ws.value(row, col, list.get(rowId).getFraudStatus());
//                    ws.formula(row, col,
//                            String.format("IFERROR(VLOOKUP(\"%d\", Config!$A$8:$B$12, 2, 0),\"\")"
//                                    , list.get(rowId).getFraudStatus()));
//                    ws.style(row, col)
//                            .fontSize(12)
//                            .borderStyle(BorderStyle.THIN)
//                            .fontColor("110102")
//                            .fontName(TIMES_NEW_ROMAN)
//                            .verticalAlignment(CENTER)
//                            .horizontalAlignment(LEFT)
//                            .set();
//                }
//            }
//            row++;
//        }
//        //try {
//        //    ws.finish();
//        //} catch (Exception e) {
//        //    log.debug("Write data to sheet has error", e);
//        //}
//        log.info("End push data to sheet excel success");
//        MDC.clear();
//    }

    public static void createWorksheetConfig(org.dhatim.fastexcel.Worksheet wsConfig) {
//        MDC.put(TOKEN, SequenceGenerator.nextStringId());
        log.info("Begin create sheet config");
//        /Sheet config /
        wsConfig.setVisibilityState(VisibilityState.HIDDEN);
        wsConfig.range(0, 0, 0, 1).merge();
        wsConfig.range(6, 0, 6, 1).merge();
        wsConfig.range(13, 0, 13, 1).merge();
        wsConfig.range(22, 0, 22, 1).merge();
        wsConfig.range(30, 0, 30, 1).merge();
        wsConfig.range(39, 0, 39, 1).merge();
        wsConfig.range(46, 0, 46, 1).merge();
        wsConfig.range(51, 0, 51, 1).merge();
        wsConfig.range(51, 3, 51, 4).merge();
        wsConfig.range(51, 6, 51, 7).merge();

        // Dich vu
        wsConfig.value(0, 0, "Service");
        wsConfig.value(1, 0, "1");
        wsConfig.value(1, 1, "QRVNPAY");
        wsConfig.value(2, 0, "2");
    }
}