package com.tool.sms.fileprocessing.constant;

import java.nio.file.FileSystems;

public class AppConst {
    private AppConst() {};

    public static final String ROOT_PATH = FileSystems.getDefault().getPath("").toAbsolutePath().getParent().toString();
    public static final String SENT_DIRECTORY_PATH = ROOT_PATH + "/sent/";
    public static final String PROCESSING_DIRECTORY_PATH = ROOT_PATH + "/processing/";
    public static final String ARCHIVE_DIRECTORY_PATH = ROOT_PATH + "/archive/";
    public static final String THIRD_PARTY_API = "https://smsgw.vnpaytest.vn/smsgw/sendSms";
    public static final String DATE_TIME_FORMAT = "dd:MM:yyyy HH:mm:ss";
    public static final int SIZE_THREAD = 3;
    public static final String EXCEL_FILE_EXTENSION = ".xlsx";
}
