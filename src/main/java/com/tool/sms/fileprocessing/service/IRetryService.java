package com.tool.sms.fileprocessing.service;

import com.tool.sms.fileprocessing.exception.CustomException;

import java.util.function.Supplier;

public interface IRetryService {
    <R> R retry(Supplier<R> supplier, int limit) throws CustomException;
}
