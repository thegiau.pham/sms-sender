package com.tool.sms.fileprocessing.service.impl;

import com.tool.sms.fileprocessing.exception.CustomException;
import com.tool.sms.fileprocessing.service.IRetryService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

@Service
@RequiredArgsConstructor
public class RetryServiceImpl implements IRetryService {
    private static final int MAX_RETRY_COUNT = 3;
    private static final int TIME_DELAY = 3000;
    private static final Logger LOGGER = LoggerFactory.getLogger(RetryServiceImpl.class);
    @Override
    public <R> R retry(Supplier<R> supplier, int limit) throws CustomException {
        return retry(supplier, 0, limit,null);
    }

    private <R> R retry(Supplier<R> supplier, int count, int limit, Exception lastException) throws CustomException{
        if(count >= limit) {
            LOGGER.info("Retry max attempt limit {}, retry count {} , retry error", limit, count);
            throw new CustomException(lastException);
        }
        try {
            return supplier.get();
        }
        catch (Exception ex) {
            try {
                TimeUnit.MICROSECONDS.sleep(TIME_DELAY);
            }
            catch (InterruptedException e) {
                LOGGER.error("Delay transaction error", e);
            }
            LOGGER.info("Retry times: {}, delay {}", count + 1, TIME_DELAY);
            return retry(supplier, ++count, limit, ex);
        }
    }
}
