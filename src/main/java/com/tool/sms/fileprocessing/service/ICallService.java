package com.tool.sms.fileprocessing.service;

import com.tool.sms.fileprocessing.dto.RequestDTO;
import com.tool.sms.fileprocessing.exception.CustomException;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;

import java.util.List;

public interface ICallService <K, T>{
    @Retryable(
            value = CustomException.class,
            maxAttemptsExpression = "${spring.retry.attempt}",
            backoff = @Backoff(maxDelayExpression = "${spring.retry.backoff-period}")
    )
    void callApiWithRetry (T object, String url) throws CustomException, RuntimeException;
    @Recover
    void recoverCallApi (CustomException ex, T Object, String url);
}
