package com.tool.sms.fileprocessing.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tool.sms.fileprocessing.constant.AppConst;
import com.tool.sms.fileprocessing.dto.RequestDTO;
import com.tool.sms.fileprocessing.dto.ResponseDTO;
import com.tool.sms.fileprocessing.enums.SmsResponseType;
import com.tool.sms.fileprocessing.exception.CustomException;
import com.tool.sms.fileprocessing.service.ICallService;
import com.tool.sms.fileprocessing.utils.NetworkUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CallServiceImpl implements ICallService<Object, RequestDTO> {
    private static final Logger LOGGER = LoggerFactory.getLogger(CallServiceImpl.class);
    @Override
//    @Async("threadPoolTaskExecutor")
    public void callApiWithRetry(RequestDTO object, String url) throws CustomException{
        LOGGER.info("Method: Post, Url: {}, Data: {}", url, object.toString());
        ResponseEntity<Object> responseEntity = NetworkUtils.getInstance().doPost(object, AppConst.THIRD_PARTY_API);
        if(responseEntity.getStatusCode() != HttpStatus.OK) {
            // TODO: handle smt
        }
        ObjectMapper objectMapper = new ObjectMapper();
        ResponseDTO responseDTO = objectMapper.convertValue(responseEntity.getBody(), ResponseDTO.class);

        if(!SmsResponseType.SUCCESS.getCode().equals(responseDTO.getStatus())) {
            SmsResponseType smsType = Arrays.stream(SmsResponseType.values())
                    .filter(type -> type.getCode().equals(responseDTO.getStatus()))
                    .findFirst()
                    .orElseGet(() -> SmsResponseType.SERVER_ERROR);
            throw new CustomException(smsType.getCode(), smsType.getMessage());
        }
    }

    @Override
    public void recoverCallApi(CustomException ex, RequestDTO Object, String url) {
        LOGGER.info("Exception handle recover: {}", ex.getMessage());
//        return new ResponseEntity<>(new ResultEntity(
//                SmsResponseType.SERVER_ERROR.getCode(),
//                SmsResponseType.SERVER_ERROR.getMessage()
//        ), HttpStatus.OK);
    }
}
