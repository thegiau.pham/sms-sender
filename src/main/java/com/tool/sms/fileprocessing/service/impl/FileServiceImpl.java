package com.tool.sms.fileprocessing.service.impl;

import com.tool.sms.fileprocessing.constant.AppConst;
import com.tool.sms.fileprocessing.dto.RequestDTO;
import com.tool.sms.fileprocessing.exception.CustomException;
import com.tool.sms.fileprocessing.service.IFileService;
import com.tool.sms.fileprocessing.utils.FileUtils;
import com.tool.sms.fileprocessing.utils.NetworkUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service
public class FileServiceImpl implements IFileService <RequestDTO> {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileServiceImpl.class);

    private final CallServiceImpl callService;
    private final RetryServiceImpl retryService;

    public FileServiceImpl(CallServiceImpl callService, RetryServiceImpl retryService) {
        this.callService = callService;
        this.retryService = retryService;
    }

    @Override
    public Hashtable<String, List<RequestDTO>> readFiles(String pathDirectory) throws ExecutionException, InterruptedException {
        long startTime = System.currentTimeMillis();
        List<File> lstFiles = FileUtils.getInstance().listFilesUsingJavaIO(pathDirectory);
        Hashtable<String, List<RequestDTO>> htbRequest = new Hashtable<>();
        CompletableFuture.allOf(lstFiles.stream()
                .map(file -> FileUtils.getInstance().readFileExcel(file).thenAccept(lstRequestDto -> htbRequest.put(file.getName(), lstRequestDto)))
                .toArray(CompletableFuture[]::new)).get();
        LOGGER.info("Number of file: {} - Reading time in ms: {}", lstFiles.size(), System.currentTimeMillis() - startTime);
        return htbRequest;
    }

    @Override
//    @Async("threadPoolTaskExecutor")
    public ResponseEntity<Object> handleProcessing(String fileName, List<RequestDTO> list, int sizeThread) throws Exception {
        int totalThread = list.size() / sizeThread + 1;
        int indexList = 0;
        int size;
        long startTime = System.currentTimeMillis();
        if(totalThread == 1) {
            size = list.size();
        }
        else size = sizeThread;
        ArrayList<CompletableFuture<Void>> futures = new ArrayList<>();
        for(int i = 0; i < totalThread; i ++) {
            int finalIndex = indexList;
            int finalSize = size;
            if(finalSize == finalIndex) continue;
            CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
                for (RequestDTO dto: list.subList(finalIndex, finalSize)) {
                    try {
                        callService.callApiWithRetry(dto, AppConst.THIRD_PARTY_API);
                    } catch (CustomException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
            futures.add(future);
            indexList+=sizeThread;
            if(indexList + sizeThread > list.size()) {
                size = list.size();
            }
            else {
                size += sizeThread;
            }
        }

        CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).get();
        LOGGER.info("Thread count: {}", Thread.activeCount());
        LOGGER.info("Time processing: {}", System.currentTimeMillis() - startTime);
        LOGGER.info("Time call api: {}", NetworkUtils.getInstance().getCounter());
        return new ResponseEntity<>(HttpStatus.PROCESSING);
    }
}
