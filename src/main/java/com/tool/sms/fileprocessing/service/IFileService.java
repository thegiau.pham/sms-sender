package com.tool.sms.fileprocessing.service;

import com.tool.sms.fileprocessing.dto.RequestDTO;
import org.springframework.http.ResponseEntity;

import java.io.File;
import java.io.IOException;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.ExecutionException;

public interface IFileService <T> {
    Hashtable<String, List<T>> readFiles(String pathDirectory) throws Exception;
    ResponseEntity<?> handleProcessing(String fileName, List<RequestDTO> list, int sizeThread) throws Exception;
}
