//package com.tool.sms.fileprocessing.config;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.retry.backoff.FixedBackOffPolicy;
//import org.springframework.retry.policy.SimpleRetryPolicy;
//import org.springframework.retry.support.RetryTemplate;
//
//@Configuration
//public class RetryTemplateConfig {
//    @Value("${spring.retry.attempt}")
//    private int maxAttempt;
//    @Value("${spring.retry.backoff-period}")
//    private int backOffPeriod;
//    @Bean
//    public RetryTemplate getRetryTemplate () {
//        SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy();
//        retryPolicy.setMaxAttempts(this.maxAttempt);
//        FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();
//        backOffPolicy.setBackOffPeriod(this.backOffPeriod);
//        RetryTemplate template = new RetryTemplate();
//        template.setRetryPolicy(retryPolicy);
//        template.setBackOffPolicy(backOffPolicy);
//        return template;
//    }
//}
