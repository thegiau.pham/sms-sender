package com.tool.sms.fileprocessing.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;
import java.util.concurrent.ForkJoinPool;

@Configuration
@EnableAsync(proxyTargetClass = true)
public class AsyncConfiguration {

    @Bean(name = "threadPoolTaskExecutor")
    public Executor asyncExecutor () {
//        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
//        executor.setCorePoolSize(10);
//        executor.setMaxPoolSize(50);
//        executor.setQueueCapacity(200);
//        executor.setThreadNamePrefix("AsyncThread-");
//        executor.initialize();
        ForkJoinPool executor = new ForkJoinPool(5);
        return executor;
    }
}
