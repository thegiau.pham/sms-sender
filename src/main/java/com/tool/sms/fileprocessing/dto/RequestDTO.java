package com.tool.sms.fileprocessing.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RequestDTO implements Serializable {
    private String messageId;
    private String destination;
    private String sender;
    private String keyword;
    private String shortMessage;
    private String encryptMessage;
    private int isEncrypt;
    private int type;
    private Long requestTime;
    private String partnerCode;
    private String sercretKey;
}
