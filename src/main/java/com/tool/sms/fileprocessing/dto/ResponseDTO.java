package com.tool.sms.fileprocessing.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@ToString
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseDTO implements Serializable {
    private String status;
    private String description;
    private int isMnp;
    private String sender;
    private String keyword;
    private String destination;
}
