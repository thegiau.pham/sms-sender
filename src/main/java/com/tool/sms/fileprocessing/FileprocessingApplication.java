package com.tool.sms.fileprocessing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(scanBasePackages = {"com.tool.sms.fileprocessing"})
@EnableScheduling
@EnableRetry
public class FileprocessingApplication {

	public static void main(String[] args) {
		SpringApplication.run(FileprocessingApplication.class, args);
	}

}
