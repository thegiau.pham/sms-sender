package com.tool.sms.fileprocessing.exception;

import com.tool.sms.fileprocessing.entity.ResultEntity;
import com.tool.sms.fileprocessing.enums.SmsResponseType;

public interface IErrorResult {
    default ResultEntity error (Exception ex) {
        ResultEntity result = new ResultEntity();
        result.setMessage(ex.getMessage());
        if(ex instanceof CustomException) {
            CustomException customException = (CustomException) ex;
            result.setCode(customException.getCode());
            result.setData(customException.getData());
        }
        else {
            result.setCode(SmsResponseType.SERVER_ERROR.getCode());
            result.setMessage(SmsResponseType.SERVER_ERROR.getMessage());
        }
        return result;
    }
}
