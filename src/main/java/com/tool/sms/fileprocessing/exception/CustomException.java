package com.tool.sms.fileprocessing.exception;

public class CustomException extends Exception{
    public CustomException(Exception ex) {
        super(ex);
    }

    public CustomException(String code, String message) {
        super(message);
        this.code = code;
    }

    public CustomException(String code, Exception ex) {
        super(ex);
        this.code = code;
    }

    private String code;
    private Object data;

    public void setCode (String code) {
        this.code  = code;
    }
    public String getCode () {
        return this.code;
    }
    public void setData(Object data) {
        this.data = data;
    }
    public Object getData() {
        return this.data;
    }
}
