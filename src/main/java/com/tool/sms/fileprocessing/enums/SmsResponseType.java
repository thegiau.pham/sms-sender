package com.tool.sms.fileprocessing.enums;

public enum SmsResponseType implements IResponseType {
    SERVER_ERROR("09", "Internal server error"),
    SUCCESS("00", "Thành công"),
    INVALID_MOBILENO("01", "Sai số điện thoại"),
    INVALID_LEN("02", "Độ dài không hợp lệ"),
    INVALID_AUTHEN("04", "Sai thông tin xác thực"),
    PROVIDER_NOT_CONNECT("05", "Mất kết nối đến nhà cung cấp"),
    IP_NOT_REGISTERED("06", "IP không được phép truy cập"),
    PROVIDER_TIMEOUT("08", "Timeout"),
    INVALID_TYPE("11", "Sai loại tin nhắn"),
    UNICODE_NOT_SUPPORT("12", "Không hỗ trợ tin Unicode"),
    PARTNER_NOT_FOUND("80", "Không tìm thấy đối tác"),
    PARTNER_NOT_SUPPORT("81", "Đối tác chưa được hỗ trợ"),
    PROVIDER_NOT_FOUND("82", "Không tìm thấy nhà cung cấp"),
    PROVIDER_NOT_SUPPORT("83", "Nhà cung cấp chưa được hỗ trợ"),
    ROUTING_NOT_FOUND("84", "Chưa định tuyển dịch vụ"),
    INVALID_SENDER("85", "Sai sender"),
    INVALID_KEYWORD("86", "Sai từ khóa"),
    INVALID_TEMPLATE("87", "Sai mẫu tin nhắn"),
    MESSAGE_NOT_ENCRYPT("88", "Thuê bao Viettel chưa được mã hóa"),
    MESSAGE_ENCRYPT("89", "Thuê bao mạng khác Viettel những đã mã hóa."),
    INVALID_DATA("97", "Sai dữ liệu đầu vào"),
    DUPLICATE_TRANSACTION("90", "Tin nhắn trùng lặp"),
    EXCEPTION("99", "Lỗi ngoại lệ");

    private final String code;
    private final String message;

    SmsResponseType(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
