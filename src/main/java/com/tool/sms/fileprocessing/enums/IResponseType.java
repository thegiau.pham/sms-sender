package com.tool.sms.fileprocessing.enums;

public interface IResponseType {
    String getCode();
    String getMessage();
}
