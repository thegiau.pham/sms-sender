package com.tool.sms.fileprocessing.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResultEntity {

    public ResultEntity(String code, String message) {
        this.code = code;
        this.message = message;
    }

    private String code;
    private String message;
    private Object data;
}
